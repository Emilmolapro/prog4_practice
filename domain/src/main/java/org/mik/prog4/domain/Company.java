package org.mik.prog4.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@SuperBuilder
@Entity
@Table(name = Company.TBL_NAME)
public class Company extends Client{

    public static final String TBL_NAME="company";
    public static final String FLD_TAX_ID="tax_id";

    @NotNull
    @Size(min = 11, max = 11)
    @Column(name = FLD_TAX_ID, nullable = false, unique = true)
    private String taxId;

}
