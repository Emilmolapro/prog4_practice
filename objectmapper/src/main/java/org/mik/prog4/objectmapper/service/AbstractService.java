package org.mik.prog4.objectmapper.service;

import org.mik.prog4.domain.Client;

public abstract class AbstractService<T extends Client> {

    abstract void pay(T client);

    abstract void receiveService(T client);

}
