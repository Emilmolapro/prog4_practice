package org.mik.prog4.database.hibernate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.mik.prog4.database.repository.CompanyRepository;
import org.mik.prog4.database.repository.CountryRepository;
import org.mik.prog4.database.repository.PersonRepository;
import org.mik.prog4.domain.Client;
import org.mik.prog4.domain.Company;
import org.mik.prog4.domain.Country;
import org.mik.prog4.domain.Person;

import javax.transaction.UserTransaction;
import java.util.Properties;

public class HibernateUtil {

    private static final Logger LOG = LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static SessionFactory sessionFactory;

    public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration cfg = new Configuration();
                Properties properties = new Properties();
                properties.put(Environment.DRIVER, "org.hsqldb.jdbc.JDBCDriver");
                properties.put(Environment.URL, "jdbc:hsqldb:mem:db/mp;hsqldb.log_data=false");
                properties.put(Environment.USER, "sa");
                properties.put(Environment.PASS, "");
                properties.put(Environment.DIALECT, "");
                properties.put(Environment.SHOW_SQL, "false");
                properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                properties.put(Environment.ALLOW_UPDATE_OUTSIDE_TRANSACTION, "true");
                properties.put(Environment.HBM2DDL_AUTO, "create-drop");
                properties.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS, "true");
                cfg.setProperties(properties);
                cfg.addAnnotatedClass(Person.class);
                cfg.addAnnotatedClass(Client.class);
                cfg.addAnnotatedClass(Company.class);
                cfg.addAnnotatedClass(Country.class);
                ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(cfg.getProperties()).build();
                sessionFactory = cfg.buildSessionFactory(registry);
                initDb();
            } catch (Exception e) {
                LOG.error(e);
                System.exit(-1);
            }
        }
        return sessionFactory;
    }

    private static void initDb() throws Exception {
        initCountries();
        initPersons();
        initCompanies();
    }

    private static void initCountries() throws Exception {
        CountryRepository cr = CountryRepository.getInstance();
        cr.save(Country.builder()
                .name("Hungary")
                .sign("H")
                .build());
        cr.save(Country.builder()
                .name("USA")
                .sign("US")
                .build());
        cr.save(Country.builder()
                .name("Great Britain")
                .sign("GB")
                .build());
        cr.save(Country.builder()
                .name("Betelgeuse")
                .sign("BET")
                .build());
    }

    private static void initPersons() throws Exception {
        PersonRepository pr = PersonRepository.getInstance();
        CountryRepository cr = CountryRepository.getInstance();
        pr.save(Person.builder()
                .name("Zaphod Beeblebrox")
                .address("Betelgeuse")
                .personalId("ZB12345678")
                .country(cr.getByName("Betelgeuse").get(0))
                .build());
        pr.save(Person.builder()
                .name("Tricia McMillan")
                .address("London")
                .personalId("TM12345678")
                .country(cr.getByName("Great Britain").get(0))
                .build());
        pr.save(Person.builder()
                .name("Ford Prefect")
                .address("Betelgeuse")
                .personalId("FP12345678")
                .country(cr.getByName("Betelgeuse").get(0))
                .build());
    }

    private static void initCompanies() throws Exception {
        CountryRepository cr = CountryRepository.getInstance();
        CompanyRepository cor = CompanyRepository.getInstance();
        cor.save(Company.builder()
                        .name("Microsoft")
                        .address("Redmond")
                        .country(cr.getByName("USA").get(0))
                        .taxId("MS12345678")
                .build());
        cor.save(Company.builder()
                .name("Google")
                .address("Frisco")
                .country(cr.getByName("USA").get(0))
                .taxId("GG12345678")
                .build());
    }

}
