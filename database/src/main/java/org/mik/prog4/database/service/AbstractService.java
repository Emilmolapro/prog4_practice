package org.mik.prog4.database.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.database.repository.AbstractRepository;
import org.mik.prog4.domain.AbstractEntity;

import java.io.Serializable;
import java.util.List;

public abstract class AbstractService<ID extends Serializable, E extends AbstractEntity<ID>> {
     private static final Logger LOG= LogManager.getLogger();

     protected abstract AbstractRepository<ID, E> getRepository();

     public E findById(ID id) throws Exception {
         return getRepository().getById(id);
     }

     public List<E> findAll() throws Exception {
         return getRepository().getAll();
     }

    public List<E> findAll(int currentPage, int recordPerPage) throws Exception {
        return getRepository().getAll(currentPage, recordPerPage);
    }

}
