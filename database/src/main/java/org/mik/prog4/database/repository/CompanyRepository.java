package org.mik.prog4.database.repository;

import org.hibernate.Query;
import org.mik.prog4.domain.Company;
import org.mik.prog4.domain.Company;

public class CompanyRepository extends AbstractRepository<Long, Company> {

    private static CompanyRepository instance;

    public static synchronized CompanyRepository getInstance() {
        if (instance==null)
            instance=new CompanyRepository();
        return instance;
    }
    private CompanyRepository() {}

    @Override
    protected Class<Company> getClazz() {
        return Company.class;
    }

    public Company getByTaxId(String id) throws Exception {
        if (id==null || id.isEmpty())
            return null;
        return doIt((((session, tr) -> {
            Query<Company> query=createCriteria(session, Company.class, (cb, r, cq)-> {
                cq.select(r).where(cb.equal(r.get(Company.FLD_TAX_ID), Long.getLong(id)));
            });
            return query.getSingleResult();
        })));
    }
}
