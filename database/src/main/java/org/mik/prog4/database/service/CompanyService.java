package org.mik.prog4.database.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.database.repository.AbstractRepository;
import org.mik.prog4.database.repository.CompanyRepository;
import org.mik.prog4.domain.Company;

public class CompanyService extends AbstractService<Long, Company> {
     private static final Logger LOG= LogManager.getLogger();

    private static CompanyService instance;

    public static synchronized CompanyService getInstance() {
        if (instance==null)
            instance =new CompanyService();
        return instance;
    }

    private CompanyService(){}
    @Override
    protected AbstractRepository<Long, Company> getRepository() {
        return CompanyRepository.getInstance();
    }

    public void pay(Company client) {
        LOG.info(String.format("Enter Company.pay for client %s", client));
    }

    public void receive(Company client) {
        LOG.info(String.format("Enter Company.receive from client %s", client));
    }
}
