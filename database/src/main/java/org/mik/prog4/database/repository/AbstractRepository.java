package org.mik.prog4.database.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.mik.prog4.database.hibernate.HibernateUtil;
import org.mik.prog4.domain.AbstractEntity;
import org.mik.prog4.domain.Client;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public abstract class AbstractRepository<ID extends Serializable, E extends AbstractEntity<ID>> {

    interface CriteriaSettings<R, T> {
        void create(CriteriaBuilder cb, Root<T> root, CriteriaQuery<R> query);
    }

    interface SqlAction<R> {
        R process(Session session, Transaction tr) throws Exception;
    }

    protected abstract Class<E> getClazz();

    protected <R> R doIt(SqlAction<R> action) throws Exception {
        try(Session session= HibernateUtil.getSessionFactory().getCurrentSession()) {
            Transaction tr=session.beginTransaction();
            try {
                return action.process(session,tr);
            }
            catch (Exception e) {
                tr.rollback();
                throw e;
            }
            finally {
                if (tr.getStatus().equals(TransactionStatus.ACTIVE))
                    tr.commit();
            }
        }
    }

    public void save(E entity) throws Exception {
        if (entity==null)
            return;

        doIt(((session, tr) -> {
            session.save(entity);
            return entity;
        }));
    }

    public E getById(ID id) throws Exception {
        if (id==null)
            return null;

        return doIt(((session, tr)-> session.find(getClazz(),id)));
    }

    public List<E> getByName(String name) throws Exception {
        if (name==null || name.isEmpty())
            return Collections.emptyList();

        return doIt(((session, tr)-> {
            Query<E> query = createCriteria(session, getClazz(), (cb, r, cq)-> {
               cq.select(r).where(cb.equal(r.get(Client.FLD_NAME), name));
            });
            return query.getResultList();
        }));
    }

    public List<E> getAll() throws Exception {
        return doIt(((session, tr)-> {
            org.hibernate.Query<E> query=createCriteria(session, getClazz(), (cb, r, cq)-> {
                cq.select(r);
            });
            return query.getResultList();
        }));
    }

    public List<E> getAll(int currentPage, int recordsPerPage) throws Exception {
        return doIt(((session, tr)-> {
            org.hibernate.Query<E> query=createCriteria(session, getClazz(), (cb, r, cq)-> {
                cq.select(r);
            });
            query.setFirstResult(currentPage*recordsPerPage);
            query.setMaxResults(recordsPerPage);
            return query.getResultList();
        }));
    }

    public void delete(ID id) throws Exception {
        if (id==null)
            return;

        SqlAction<E> action=(((session, tr)-> {
            E obj=session.find(getClazz(), id);
            if (obj==null)
                return null;
            session.delete(obj);
            return null;
        }));
        doIt(action);
    }

    public long getCount() throws Exception {
        SqlAction<Long> action=(((session, tr)-> {
            Query<Long> query=createCriteria(session, Long.class, (cb, r, cq)-> {
                cq.select(cb.count(r.get(AbstractEntity.FLD_ID)));
            });
            return query.getSingleResult();
        }));
        return doIt(action);
    }

    public void update(E entity) throws Exception {
        if (entity==null)
            return;

        doIt((((session, tr) -> {
            session.update(entity);
            return null;
        })));
    }

    public void saveOrUpdate(E entity) throws Exception {
        if (entity==null)
            return;

        doIt((((session, tr) -> {
            session.saveOrUpdate(entity);
            return null;
        })));
    }

    protected <R>  org.hibernate.Query<R> createCriteria(Session session, Class<R> resultClass, CriteriaSettings<R, E> criteriaSettings) {
        CriteriaBuilder cb=session.getCriteriaBuilder();
        CriteriaQuery<R> cq=cb.createQuery(resultClass);
        Root<E> root=cq.from(getClazz());
        criteriaSettings.create(cb, root, cq);
        return session.createQuery(cq);
    }

}
















