package org.mik.prog4.database.repository;

import org.mik.prog4.domain.Country;

public class CountryRepository extends AbstractRepository<Long, Country> {

    private static CountryRepository instance;

    public static synchronized CountryRepository getInstance() {
        if (instance==null)
            instance=new CountryRepository();
        return instance;
    }
    private CountryRepository() {}

    @Override
    protected Class<Country> getClazz() {
        return Country.class;
    }
}
