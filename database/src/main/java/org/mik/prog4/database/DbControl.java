package org.mik.prog4.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.prog4.database.service.CompanyService;
import org.mik.prog4.domain.Company;

import java.util.List;

public class DbControl {
     private static final Logger LOG= LogManager.getLogger();
     private CompanyService companyService=CompanyService.getInstance();

     public void start() {
         try {
             this.companyService.findAll()
                     .stream()
                     .forEach(c-> LOG.info(c));
         }
         catch (Exception e) {
             LOG.error(e.getMessage());
         }
     }
}
