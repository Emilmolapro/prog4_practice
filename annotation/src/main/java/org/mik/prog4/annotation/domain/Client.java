package org.mik.prog4.annotation.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.mik.prog4.annotation.export.json.JSonElement;
import org.mik.prog4.annotation.export.json.JSonSerializable;
import org.mik.prog4.annotation.export.xml.XMLElement;
import org.mik.prog4.annotation.export.xml.XMLSerializable;


@Data
@NoArgsConstructor
@SuperBuilder
@XMLSerializable
@JSonSerializable
public class Client {
        @XMLElement
        @JSonElement
        private String name;
        @XMLElement
        @JSonElement
        private String address;
        @XMLElement
        @JSonElement
        private Country country;
}
