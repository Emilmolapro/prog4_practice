package org.mik.prog4.annotation.domain;

import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.mik.prog4.annotation.export.json.*;
import org.mik.prog4.annotation.export.xml.XMLElement;
import org.mik.prog4.annotation.export.xml.XMLSerializable;

@NoArgsConstructor
@SuperBuilder
@ToString(callSuper = true)
@XMLSerializable
@JSonSerializable
public class Company extends Client {
    @XMLElement
    @JSonElement
    private String taxId;

}
