package org.mik.prog4.annotation.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.mik.prog4.annotation.export.json.JSonElement;
import org.mik.prog4.annotation.export.json.JSonSerializable;
import org.mik.prog4.annotation.export.xml.XMLElement;
import org.mik.prog4.annotation.export.xml.XMLSerializable;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@XMLSerializable
@JSonSerializable
public class Person extends Client {

    @XMLElement
    @JSonElement
    private String personalId;

}
