package org.mik.prog4.annotation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.mik.prog4.annotation.export.json.JSonElement;
import org.mik.prog4.annotation.export.json.JSonSerializable;
import org.mik.prog4.annotation.export.xml.XMLElement;
import org.mik.prog4.annotation.export.xml.XMLSerializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@XMLSerializable
@JSonSerializable
public class Country {

    @XMLElement
    @JSonElement
    private String name;
    @XMLElement
    @JSonElement
    private String sign;

}
